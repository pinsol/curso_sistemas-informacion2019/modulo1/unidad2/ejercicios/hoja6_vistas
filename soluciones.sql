﻿                                                /** MODULO I - UNIDAD II - Hoja 6 VISTAS **/

USE m1u2h6;

/*
  Crear una vista que debe mostrar los pedidos de los clientes que son de Madrid, Barcelona o Zaragoza.
  Visualizar los campos: CÓDIGO CLIENTE, NUMERO DE PEDIDO, EMPRESA y POBLACIÓN. Llamar a la vista CONSULTA1.
*/
CREATE OR REPLACE VIEW CONSULTA1a AS      -- clientes de Madrid, Barcelona y Zaragoza
  SELECT
    c.`CÓDIGO CLIENTE`,
    c.EMPRESA,
    c.POBLACIÓN
  FROM
    clientes c 
  WHERE
    c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza')
;

CREATE OR REPLACE VIEW CONSULTA1b AS    -- pedidos de clientes
  SELECT
    p.`NÚMERO DE PEDIDO`,
    p.`CÓDIGO CLIENTE` 
  FROM
    pedidos p
;

CREATE OR REPLACE VIEW CONSULTA1 AS
  SELECT
    b.`NÚMERO DE PEDIDO`,
    a.`CÓDIGO CLIENTE`,
    a.EMPRESA,
    a.POBLACIÓN 
  FROM CONSULTA1a a
  JOIN CONSULTA1b b USING(`CÓDIGO CLIENTE`)
;

SELECT * FROM CONSULTA1 c;


/*
  Qué clientes han realizado algún pedido en el año 2002 y su forma de pago fue tarjeta.
  Utilizamos los campos EMPRESA y POBLACIÓN de la tabla CLIENTES, y
  NÚMERO DE PEDIDO, FECHA DE PEDIDO y FORMA DE PAGO de la tabla PEDIDOS.
  Ordenar los registros por el campo FECHA DE PEDIDO de forma ascendente.
  Guardamos la vista con el nombre CONSULTA 2
*/
CREATE OR REPLACE VIEW CONSULTA2a AS
  SELECT
    p.`CÓDIGO CLIENTE`
  FROM
    pedidos p 
  WHERE
    YEAR(p.`FECHA DE PEDIDO`)= 2002 
    AND p.`FORMA DE PAGO`= 'TARJETA'
;